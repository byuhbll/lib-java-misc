package edu.byu.hbll.misc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class consists exclusively of static convenience or utility methods related to common list
 * operations.
 */
public class Lists {

  /**
   * Returns the first element of a list, or an empty {@link Optional} if the list is empty (or if
   * the first element of the list is null).
   *
   * @param <T> the type of elements in the list
   * @param list the list for which to retrieve the first element; may be null
   * @return the first element as described
   */
  public static <T> Optional<T> first(List<T> list) {
    return (list == null || list.isEmpty()) ? Optional.empty() : Optional.ofNullable(list.get(0));
  }

  /**
   * Returns the last element of a list, or an empty {@link Optional} if the list is empty (or if
   * the last element of the list is null).
   *
   * @param <T> the type of elements in the list
   * @param list the list for which to retrieve the last element; may be null
   * @return the last element as described
   */
  public static <T> Optional<T> last(List<T> list) {
    return (list == null || list.isEmpty())
        ? Optional.empty()
        : Optional.ofNullable(list.get(list.size() - 1));
  }

  /**
   * Returns a new, immutable list containing all the members of the original list (if non-null),
   * appended with all the given elements.
   *
   * <p>If a null or empty list is provided, then the new list will only contain the new elements.
   *
   * @param <T> the type of object contained in the list
   * @param list the original list; may be null
   * @param elements the new elements to add
   * @return a new list representing the concatentation of the original list and the new elements
   */
  @SafeVarargs
  public static <T> List<T> with(List<T> list, T... elements) {
    if (list == null || list.isEmpty()) {
      return elements.length == 0 ? Collections.emptyList() : Arrays.asList(elements);
    } else  {
      return Stream.concat(list.stream(), Stream.of(elements)).collect(Collectors.toList());
    }
  }

  /**
   * Constructs a new instance; since this is a utility class, this constructor will never be used.
   */
  private Lists() {
    // Do nothing.
  }
}
