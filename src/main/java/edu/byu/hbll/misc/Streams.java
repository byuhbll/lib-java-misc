package edu.byu.hbll.misc;

import java.util.Iterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This class consists exclusively of static convenience or utility methods related to
 * {@link Stream}.
 */
public class Streams {

  /**
   * Creates a new sequential (not parallel) stream from an {@link Iterable}.
   *
   * @param <T> the type of iterable elements
   * @param iterable the iterable describing the stream elements
   * @return the new sequential stream
   */
  public static <T> Stream<T> of(Iterable<T> iterable) {
    return StreamSupport.stream(iterable.spliterator(), false);
  }

  /**
   * Creates a new sequential (not parallel) stream from an {@link Iterator}.
   * 
   * @param <T> the type of iterator elements
   * @param iterator the iterator describing the stream elements
   * @return the new sequential stream
   */
  public static <T> Stream<T> of(Iterator<T> iterator) {
    return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false);
  }

  /**
   * Constructs a new instance; since this is a utility class, this constructor will never be used.
   */
  private Streams() {
    // Do nothing.
  }
}
