package edu.byu.hbll.misc;

import java.math.BigInteger;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A monitor that can be used to coordinate between multiple instances of an application.
 *
 * <p>Many applications can have multiple instances running to support high availability, but still
 * only allow one of many instances to do background processing on data. This class provides a
 * simple way to coordinate which of the application instances should perform the processing.
 *
 * <p>The {@code PrimaryMonitor} should be shutdown by calling {@code shutdown()} to terminate the
 * internal thread. Alternatively, a {@code ManagedThreadFactory} may be provided to the {@code
 * Builder}.
 *
 * <p>Unless specific circumstances require it, a single {@code PrimaryMonitor} should be shared
 * throughout an entire application instance. Creating multiple {@code PrimaryMonitor}s could result
 * in only specific portions of code functioning correctly.
 *
 * <p>Usage is straightforward:
 *
 * <pre>
 * Initial setup:
 * {@code
 * PrimaryMonitorDatabase db = new MongoPrimaryMonitorDatabase();
 * PrimaryMonitor monitor = new PrimaryMonitor.Builder(db).build();
 * }
 *
 * Then, at the beginning of a scheduled harvest:
 * {@code
 * if (!monitor.isPrimary()){
 *   return;
 * }
 * }
 * </pre>
 *
 * @author bwelker
 */
public class PrimaryMonitor implements Runnable {

  private static final Logger logger = Logger.getLogger(PrimaryMonitor.class.getName());

  /** The default threshold for becoming the primary. */
  public static final int DEFAULT_THRESHOLD_COUNT = 3;
  /** The default number of bits to use in the database key */
  public static final int DEFAULT_RANDOM_BITS = 128;

  /** Whether or not a PrimaryMonitor instance has already been created. */
  private static boolean initialized = false;

  /** The threshold for becoming the primary. */
  private int thresholdCount;
  /** The number of bits to use in the database key. */
  private int randomBits;
  /** The database to use. */
  private PrimaryMonitorDatabase database;
  /** The key found in the database during the previous execution of {@code run()}. */
  private volatile String oldKey;
  /** The key generated during the previous execution of {@code run()}. */
  private volatile String myOldKey;
  /** The number of executions the database key hasn't changed. */
  private volatile long staleCount = 0;
  /** The number of executions this has been the primary. */
  private volatile long primaryCount = 0;
  /** Whether or not this is the primary. */
  private volatile boolean primary = false;
  /**
   * The {@link ScheduledExecutorService} used to periodically execute {@code run()} on this object.
   */
  private ScheduledExecutorService pool;

  /**
   * Constructs a new {@code PrimaryMonitor} using the options in the provided {@code Builder}.
   *
   * @param builder the {@code Builder} with options for this {@code PrimaryMonitor}
   */
  private PrimaryMonitor(Builder builder) {
    if (initialized) {
      logger.warning("An instance of PrimaryMonitor has already been created in this application.");
    }
    initialized = true;

    this.thresholdCount = builder.thresholdCount;
    this.randomBits = builder.randomBits;
    this.database = builder.database;

    pool = Executors.newScheduledThreadPool(1, builder.threadFactory);
    pool.scheduleAtFixedRate(this, 0, 1, TimeUnit.SECONDS);
  }

  /**
   * {@inheritDoc}
   *
   * <p>Checks the key in the database to determine if there is an active primary and whether or not
   * this should become the primary.
   */
  @Override
  public void run() {
    String newKey;
    try {
      newKey = database.readValue();
    } catch (Exception e) {
      logger.log(Level.SEVERE, "Unable to read existing key from database", e);
      primary = false;
      return;
    }
    String myNewKey = new BigInteger(randomBits, new Random()).toString(16);

    if (newKey.equals(myOldKey)) {
      staleCount = 0;
      primaryCount++;
    } else if (newKey.equals(oldKey)) {
      staleCount++;
      primaryCount = 0;
    } else {
      staleCount = 0;
      primaryCount = 0;
    }

    if (staleCount >= thresholdCount || primaryCount > 0) {
      try {
        database.writeValue(myNewKey);
      } catch (Exception e) {
        logger.log(Level.SEVERE, "Unable to write new key to database", e);
        primary = false;
        return;
      }
    }

    primary = primaryCount >= thresholdCount;
    oldKey = newKey;
    myOldKey = myNewKey;
  }

  /**
   * Whether or not this is the primary.
   *
   * @return boolean
   */
  public boolean isPrimary() {
    return primary;
  }

  /** Initiates an orderly shutdown of the internal {@link ScheduledExecutorService}. */
  public void shutdown() {
    pool.shutdown();
  }

  /**
   * Builder class for {@code PrimaryMonitor}.
   *
   * @author bwelker
   */
  public static class Builder {

    /** The threshold for becoming the primary. */
    private int thresholdCount = DEFAULT_THRESHOLD_COUNT;
    /** The number of bits to use in the database key. */
    private int randomBits = DEFAULT_RANDOM_BITS;
    /** The {@link PrimaryMonitorDatabase} to use. */
    private PrimaryMonitorDatabase database;
    /**
     * The {@link ThreadFactory} to use in constructing the internal {@link
     * ScheduledExecutorService}.
     */
    private ThreadFactory threadFactory = Executors.defaultThreadFactory();

    /**
     * Constructs a new {@code Builder} with the provided {@link PrimaryMonitorDatabase}.
     *
     * @param database The {@link PrimaryMonitorDatabase} to use.
     */
    public Builder(PrimaryMonitorDatabase database) {
      this.database = Objects.requireNonNull(database);
    }

    /**
     * Sets the threshold for becoming the primary.
     *
     * <p>If the current primary fails, the {@code PrimaryMonitor} will wait {@code thresholdCount}
     * seconds before attempting to become the new primary.
     *
     * @param thresholdCount the number of seconds to wait before attempting to become the new
     *     primary.
     * @return Builder
     */
    public Builder thresholdCount(int thresholdCount) {
      this.thresholdCount = thresholdCount;
      return this;
    }

    /**
     * The number of bits to use when generating a random string for the database.
     *
     * @param randomBits the number of bits.
     * @return Builder
     */
    public Builder randomBits(int randomBits) {
      this.randomBits = randomBits;
      return this;
    }

    /**
     * The {@link ThreadFactory} to use when constructing the internal {@link
     * ScheduledExecutorService}.
     *
     * @param threadFactory the {@link ThreadFactory} to use.
     * @return Builder
     */
    public Builder threadFactory(ThreadFactory threadFactory) {
      this.threadFactory = Objects.requireNonNull(threadFactory);
      return this;
    }

    /**
     * Builds a new {@link PrimaryMonitor} using the configured options.
     *
     * @return {@link PrimaryMonitor}
     */
    public PrimaryMonitor build() {
      return new PrimaryMonitor(this);
    }
  }
}
