package edu.byu.hbll.misc;

import java.time.Duration;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A type of executor service that processes collections (batches) of items in separate threads.
 * Processing items in a batch can be much more efficient than processing each item individually. As
 * items are submitted to this executor, the items are placed in a batch which is then given to a
 * separate thread to be processed. Having separate threads process the batch decouples the
 * submission and the actual execution. The executor returns a {@link Future} so the item can be
 * tracked through the process and an optional result returned.
 *
 * <p>The caller provides the code that will execute the batch during the creation of the {@link
 * BatchExecutorService}. This code or method of {@link BatchRunnable} must be thread safe.
 *
 * <p>The batch size, queue size, number of threads, etc. are configurable during the creation of
 * the {@link BatchExecutorService}.
 *
 * <p>The executor service should be shutdown in order to terminate the internal threads. Any items
 * in the queue at the point of shutdown will still be processed, but no new items are allowed to be
 * submitted.
 *
 * <p>Example:
 *
 * <pre>{@code
 * BatchRunnable<Integer, Double> r = b -> b.stream().map(e -> Math.pow(e, 2)).collect(Collectors.toList());
 * BatchExecutorService<Integer, Double> executor = new BatchExecutorService.Builder(r).build();
 *
 * Future<Double> f1 = executor.submit(1);
 * Future<Double> f2 = executor.submit(2);
 * Future<Double> f3 = executor.submit(3);
 *
 * System.out.println(f1.get());
 * System.out.println(f2.get());
 * System.out.println(f3.get());
 *
 * executor.shutdownAndWait();
 * }</pre>
 *
 * @param <E> the batch element type
 * @param <V> the result type
 */
public class BatchExecutorService<E, V> {

  private static final Logger logger = Logger.getLogger(BatchExecutorService.class.getName());

  /** Caller specified code to execute the batch. */
  private final BatchRunnable<E, V> batchRunnable;

  /** The internal queue to hold submitted elements waiting to be processed. */
  private final BlockingQueue<BatchFuture> queue;

  /** An external view of the internal work queue. */
  private final BlockingQueue<E> externalQueue;

  /** Number of items the queue can hold. */
  private final int queueCapacity;

  /** The maximum number of elements in a batch. */
  private final int batchCapacity;

  /** The maximum amount of time to delay processing while more elements are submitted. */
  private final Duration batchDelay;

  /**
   * Only allows one thread to be filling its batch at a time. This encourages fuller and fewer
   * batches.
   */
  private final ReentrantLock batchLock = new ReentrantLock();

  /** The thread filling up a batch */
  private volatile Thread fillingThread;

  /** Number of threads in the executor thread pool. */
  private final int threadCount;

  /** Amount of time to suspend each thread after executing. */
  private final Duration suspend;

  /** Used to signal when all threads have terminated. */
  private final CountDownLatch shutdownLatch;

  /** Used to signal when a shutdownElement has reached the head of the queue. */
  private final CountDownLatch queueCleared = new CountDownLatch(1);

  /** Flag indicating that shutdown has been called */
  private volatile boolean shutdownInvoked;

  /**
   * Synchronization lock to prevent race conditions between the call to shutdown and adding to the
   * queue.
   */
  private final ReentrantReadWriteLock shutdownLock = new ReentrantReadWriteLock();

  /**
   * Constructs a new {@code MongoBatcher} using the options in the provided {@code Builder}.
   *
   * @param builder {@code Builder} with options for this {@code MongoBatcher}
   */
  @SuppressWarnings("unchecked")
  protected BatchExecutorService(Builder builder) {
    batchRunnable = (BatchRunnable<E, V>) builder.batchRunnable;
    queue = new LinkedBlockingQueue<>(builder.queueCapacity);
    queueCapacity = builder.queueCapacity;
    batchCapacity = builder.batchCapacity;
    batchDelay = builder.batchDelay;
    threadCount = builder.threadCount;
    suspend = builder.suspend;
    shutdownLatch = new CountDownLatch(threadCount);
    externalQueue = new ExternalQueue();

    for (int i = 0; i < threadCount; i++) {
      Thread thread = builder.threadFactory.newThread(new BatchRunnableManager());

      if (thread == null) {
        shutdown();
        throw new NullPointerException("unable to create thread");
      }

      thread.start();
    }
  }

  /**
   * Runnable class that takes elements from the queue and sends them as a batch to the
   * batchRunnable. Exactly one instance of this class will run in each thread of the executor.
   */
  private class BatchRunnableManager implements Runnable {

    /**
     * {@inheritDoc}
     *
     * <p>Takes elements from the queue and sends them as a batch to the batchRunnable.
     */
    @Override
    public void run() {
      try {
        // run until shutdown and queue is empty
        while (!(shutdownInvoked && queue.isEmpty())) {

          // batch of items to process
          List<BatchFuture> batch = new LinkedList<>();

          // only fill one batch at a time no matter the number of threads
          batchLock.lock();

          // set this thread as the filling thread
          lockRunVoid(() -> fillingThread = Thread.currentThread());

          try {

            // stop filling batch at this point in time, -1 means wait indefinitely
            long finish = -1;

            // get items from the queue until the batch is full or the batch
            // delay is reached or shutdown
            while (batch.size() < batchCapacity) {
              BatchFuture item = null;

              if (shutdownInvoked) {
                item = queue.poll();
              } else if (finish == -1) {
                try {
                  // wait indefinitely for an item from the queue
                  item = queue.take();
                } catch (InterruptedException e) {
                  // shutdown service if interrupt comes from anywhere
                  shutdown();
                }

                finish = System.currentTimeMillis() + batchDelay.toMillis();
              } else {
                try {
                  // wait to fill up batch until batch delay reached
                  item = queue.poll(finish - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                  // shutdown service if interrupt comes from anywhere
                  shutdown();
                }
              }

              if (item == null) {
                // batch delay or shutdown reached, run the batch
                break;
              }

              // add the item to the batch
              batch.add(item);
            }
          } finally {
            // allow another thread to batch items
            lockRunVoid(() -> fillingThread = null);
            batchLock.unlock();
          }

          // interrupts are only used for breaking out of blocking queue poll
          // clear interrupts here
          Thread.interrupted();

          // if there are items to process
          if (!batch.isEmpty()) {

            // extract the batch of original items from the future wrappers
            List<E> itemBatch = batch.stream().map(f -> f.element).collect(Collectors.toList());

            try {
              // run the batch
              List<V> results = batchRunnable.run(itemBatch);

              // unwind the responses and assign them back to the correct future
              results =
                  results == null || results.isEmpty()
                      ? new ArrayList<>(Collections.nCopies(batch.size(), null))
                      : results;

              if (results.size() != batch.size()) {
                throw new RuntimeException(
                    "returned list from batch runnable must be same size as input batch");
              }

              // iterate over results
              Iterator<V> vit = results.iterator();

              // iterate over batch and assign results
              for (BatchFuture f : batch) {
                f.result = vit.next();
                f.latch.countDown();
              }
            } catch (Exception e) {
              // set exception to each future in the batch
              for (BatchFuture f : batch) {
                logger.log(Level.SEVERE, e.getMessage(), e);
                f.exception = e;
                f.latch.countDown();
              }
            }

            if (shutdownInvoked && queue.isEmpty()) {
              // signal to all other suspended threads that processing is done
              queueCleared.countDown();
            }

            // suspend thread after batch run if configured to do so
            if (!Duration.ZERO.equals(suspend)) {
              try {
                // suspend thread
                queueCleared.await(suspend.toMillis(), TimeUnit.MILLISECONDS);
              } catch (InterruptedException e) {
                // shutdown service if interrupt comes from anywhere
                shutdown();
              }
            }
          }
        }
      } finally {
        // signal that this thread has completed
        shutdownLatch.countDown();
      }
    }
  }

  /**
   * Begins the shutdown process of the {@link BatchExecutorService}. Any elements added up to this
   * point will still be processed, after which the executor's threads will terminate and stop
   * processing. Any newly added elements will be rejected.
   */
  public void shutdown() {
    shutdownLock.writeLock().lock();

    try {
      if (!shutdownInvoked) {
        if (fillingThread != null) {
          fillingThread.interrupt();
        }

        shutdownInvoked = true;
      }
    } finally {
      shutdownLock.writeLock().unlock();
    }
  }

  /**
   * Begins the shutdown process of the {@link BatchExecutorService} and awaits its termination. Any
   * elements added up to this point will still be processed, after which the executor's threads
   * will terminate and stop processing. Any newly added elements will be rejected.
   */
  public void shutdownAndWait() {
    try {
      shutdown();
      shutdownLatch.await();
    } catch (InterruptedException e) {
      // do nothing
    }
  }

  /**
   * An external view of the internal work queue. Can be used to add elements to the batch executor.
   * The queue's add, offer, and put method work similarly to the submit method except they do not
   * return a {@link Future}. Instead they honor the queue's contract for blocking, return values
   * and error handling. The queue also supports the removal of elements yet to be processed.
   *
   * @return an external view of the internal work queue
   */
  public BlockingQueue<E> getQueue() {
    return externalQueue;
  }

  /**
   * Inserts the specified element into the internal work queue, waiting if necessary for space to
   * become available.
   *
   * @param e the element to add
   * @return a {@link Future} representing result of the batch process
   * @throws InterruptedException if interrupted while waiting
   * @throws RejectedExecutionException if shutdown has been initiated
   */
  public Future<V> submit(E e) throws InterruptedException {
    BatchFuture future = new BatchFuture(e);

    boolean added =
        lockRun(
            () -> {
              queue.put(future);
              return true;
            },
            () -> {
              return false;
            });

    if (!added) {
      throw new RejectedExecutionException("The service has been shutdown.");
    }

    return future;
  }

  /**
   * Inserts the elements of the specified collection into the internal work queue, waiting if
   * necessary for space to become available.
   *
   * @param c collection containing elements to be added
   * @return list of {@link Future}'s representing results of the batch process
   * @throws InterruptedException if interrupted while waiting
   * @throws RejectedExecutionException if shutdown has been initiated
   */
  public List<Future<V>> submitAll(Collection<? extends E> c) throws InterruptedException {
    List<Future<V>> futures = new ArrayList<>();

    for (E e : c) {
      futures.add(submit(e));
    }

    return futures;
  }

  /** @return the batchRunnable */
  public BatchRunnable<E, V> getBatchRunnable() {
    return batchRunnable;
  }

  /** @return the queueCapacity */
  public int getQueueCapacity() {
    return queueCapacity;
  }

  /** @return the batchCapacity */
  public int getBatchCapacity() {
    return batchCapacity;
  }

  /** @return the batchDelay */
  public Duration getBatchDelay() {
    return batchDelay;
  }

  /** @return the threadCount */
  public int getThreadCount() {
    return threadCount;
  }

  /** @return the suspend */
  public Duration getSuspend() {
    return suspend;
  }

  /** Configures and builds a new {@link BatchExecutorService}. */
  public static class Builder {

    /** Class that executes a batch. */
    private BatchRunnable<?, ?> batchRunnable;

    /** Size of the internal queue that holds submitted elements waiting to be processed. */
    private int queueCapacity = 100;

    /** The maximum number of elements in a batch. */
    private int batchCapacity = 10;

    /** The maximum amount of time to delay processing while more elements are submitted. */
    private Duration batchDelay = Duration.ZERO;

    /** {@link ThreadFactory} to use when creating the thread pool. */
    private ThreadFactory threadFactory = Executors.defaultThreadFactory();

    /** Number of threads in the executor thread pool. */
    private int threadCount = 1;

    /** Amount of time to suspend each thread after executing. */
    private Duration suspend = Duration.ZERO;

    /**
     * Constructs a new {@link Builder} with the given class for batch execution.
     *
     * @param <E> the batch element type
     * @param <V> the result type
     * @param runnable class that executes a batch
     */
    public <E, V> Builder(BatchRunnable<E, V> runnable) {
      if (runnable == null) {
        throw new NullPointerException("runnable cannot be null");
      }

      this.batchRunnable = runnable;
    }

    /**
     * Sets the maximum size of the internal queue that holds submitted elements waiting to be
     * processed.
     *
     * <p>Default: 100
     *
     * @param queueCapacity maximum queue capacity. Must be greater than 0.
     * @return this {@code Builder}
     */
    public Builder queueCapacity(int queueCapacity) {
      if (queueCapacity < 1) {
        throw new IllegalArgumentException("queueSize must be greater than 0");
      }

      this.queueCapacity = queueCapacity;
      return this;
    }

    /**
     * Sets the maximum of elements in a batch.
     *
     * <p>Default: 10
     *
     * @param batchCapacity the max batch size. Must be greater than 0;
     * @return this {@code Builder}
     */
    public Builder batchCapacity(int batchCapacity) {
      if (batchCapacity < 1) {
        throw new IllegalArgumentException("batchSize must be greater than 0");
      }

      this.batchCapacity = batchCapacity;
      return this;
    }

    /**
     * Sets the the maximum amount of time to delay processing while more elements are submitted and
     * the batch is filling.
     *
     * <p>Default: 0
     *
     * @param batchDelay the amount of time to wait. Cannot be null.
     * @return this {@code Builder}
     */
    public Builder batchDelay(Duration batchDelay) {
      if (batchDelay == null) {
        throw new NullPointerException("batchDelay cannot be null");
      }

      this.batchDelay = batchDelay;
      return this;
    }

    /**
     * Sets the number of threads in the executor thread pool. The is equal to the number of batches
     * that can possibly be processed simultaneously.
     *
     * <p>Default: 1
     *
     * @param threadCount the number of threads in the executor thread pool
     * @return this {@code Builder}
     */
    public Builder threadCount(int threadCount) {
      if (threadCount < 1) {
        throw new IllegalArgumentException("threadCount must be greater than 0");
      }

      this.threadCount = threadCount;
      return this;
    }

    /**
     * Sets the amount of time each thread is suspended and has to wait before being allowed to
     * execute another batch. This helps to slow down processing when needed.
     *
     * <p>Default: PT0S
     *
     * @param suspend amount of time to suspend each thread after executing.
     * @return this {@code Builder}
     */
    public Builder suspend(Duration suspend) {
      Objects.requireNonNull(suspend);
      this.suspend = suspend;
      return this;
    }

    /**
     * Sets the {@link ThreadFactory} to use when creating the thread pool.
     *
     * <p>Default: Executors.defaultThreadFactory();
     *
     * @param threadFactory {@link ThreadFactory} to use
     * @return this {@code Builder}
     */
    public Builder threadFactory(ThreadFactory threadFactory) {
      if (threadFactory == null) {
        throw new NullPointerException("threadFactory cannot be null");
      }

      this.threadFactory = threadFactory;
      return this;
    }

    /**
     * Builds and starts a new {@link BatchExecutorService}.
     *
     * @param <E> the batch element type
     * @param <V> the result type
     * @return the new {@link BatchExecutorService}
     */
    public <E, V> BatchExecutorService<E, V> build() {
      return new BatchExecutorService<E, V>(this);
    }
  }

  /**
   * Wrapper around submitted elements that carries feedback information back to the submitter. This
   * information includes a result or exception and if the element has been processed.
   */
  private class BatchFuture implements Future<V> {

    /** The submitted element. */
    private final E element;

    /** The result of the processing if there is one. */
    private volatile V result;

    /** If there was an exception during processing. */
    private volatile Exception exception;

    /** Signal that the element has been processed. */
    private final CountDownLatch latch = new CountDownLatch(1);

    /**
     * Creates a new {@link BatchFuture}.
     *
     * @param e the submitted element.
     */
    private BatchFuture(E e) {
      this.element = e;
    }

    /**
     * Unsupported.
     *
     * @throws an UnsupportedOperationException
     */
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
      throw new UnsupportedOperationException();
    }

    /**
     * Canceling is not supported so this always returns false.
     *
     * @return false
     */
    @Override
    public boolean isCancelled() {
      return false;
    }

    @Override
    public boolean isDone() {
      return latch.getCount() <= 0;
    }

    @Override
    public V get() throws InterruptedException, ExecutionException {
      latch.await();

      if (exception != null) {
        throw new ExecutionException(exception);
      }

      return result;
    }

    /** {@inheritDoc} */
    @Override
    public V get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException {
      if (latch.await(timeout, unit)) {
        return get();
      } else {
        throw new TimeoutException();
      }
    }

    /** Returns the string representation of the submitted element. */
    @Override
    public String toString() {
      return element == null ? null : element.toString();
    }
  }

  /** A public view of the internal work queue. */
  private class ExternalQueue extends AbstractQueue<E> implements BlockingQueue<E> {

    @Override
    public E poll() {
      BatchFuture f = queue.poll();
      return f != null ? f.element : null;
    }

    @Override
    public E poll(long timeout, TimeUnit unit) throws InterruptedException {
      BatchFuture f = queue.poll(timeout, unit);
      return f != null ? f.element : null;
    }

    @Override
    public E peek() {
      BatchFuture f = queue.peek();
      return f != null ? f.element : null;
    }

    @Override
    public boolean offer(E e) {
      try {
        return lockRun(() -> queue.offer(new BatchFuture(e)), () -> false);
      } catch (InterruptedException e1) {
        throw new AssertionError(e1);
      }
    }

    @Override
    public void put(E e) throws InterruptedException {
      lockRun(
          () -> {
            queue.put(new BatchFuture(e));
            return false;
          },
          () -> false);
    }

    @Override
    public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException {
      return lockRun(() -> queue.offer(new BatchFuture(e), timeout, unit), () -> false);
    }

    @Override
    public E take() throws InterruptedException {
      return queue.take().element;
    }

    @Override
    public int remainingCapacity() {
      return queue.remainingCapacity();
    }

    @Override
    public int drainTo(Collection<? super E> c) {
      return drainTo(c, Integer.MAX_VALUE);
    }

    @Override
    public int drainTo(Collection<? super E> c, int maxElements) {
      List<BatchFuture> list = new ArrayList<>();
      queue.drainTo(list, maxElements);

      for (BatchFuture f : list) {
        c.add(f.element);
      }

      return list.size();
    }

    @Override
    public Iterator<E> iterator() {

      final Iterator<BatchFuture> it = queue.iterator();

      return new Iterator<E>() {

        @Override
        public boolean hasNext() {
          return it.hasNext();
        }

        @Override
        public E next() {
          return it.next().element;
        }
      };
    }

    @Override
    public int size() {
      return queue.size();
    }
  }

  /**
   * Same as to {@link Supplier}, but throws an {@link InterruptedException}.
   *
   * @author Charles Draper
   */
  @FunctionalInterface
  public interface InterruptableSupplier<T> {
    T get() throws InterruptedException;
  }

  /**
   * Executes the given runnable while the shutdown method is not executing.
   *
   * @param runnable code to execute
   */
  private void lockRunVoid(Runnable runnable) {
    // make sure shutdown is not running while executing this code
    shutdownLock.readLock().lock();

    try {
      runnable.run();
    } finally {
      shutdownLock.readLock().unlock();
    }
  }

  /**
   * Executes the 'normal' code if the service has not shutdown. Executes the 'ifShutdown' code if
   * the service has been shutdown.
   *
   * @param normal execute this if not shutdown
   * @param ifShutdown execute this IF shutdown
   * @param <T> the type of results supplied by this supplier
   * @return the result supplied by the supplier
   * @throws InterruptedException
   */
  private <T> T lockRun(InterruptableSupplier<T> normal, InterruptableSupplier<T> ifShutdown)
      throws InterruptedException {
    // make sure shutdown is not invoked while executing the code
    shutdownLock.readLock().lock();

    try {
      if (shutdownInvoked) {
        return ifShutdown.get();
      } else {
        return normal.get();
      }
    } finally {
      shutdownLock.readLock().unlock();
    }
  }
}
