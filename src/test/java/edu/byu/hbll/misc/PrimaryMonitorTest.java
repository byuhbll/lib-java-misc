package edu.byu.hbll.misc;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PrimaryMonitorTest {

  private PrimaryMonitor monitor1;
  private PrimaryMonitor monitor2;
  private PrimaryMonitorDatabase db;

  @Before
  public void init() {
    db = new StringDatabase();
  }

  @After
  public void after() {
    if (monitor1 != null) {
      monitor1.shutdown();
    }
    if (monitor2 != null) {
      monitor2.shutdown();
    }
  }

  @Test
  public void testMonitorBecomesPrimary() throws Exception {
    monitor1 = new PrimaryMonitor.Builder(db).build();
    Thread.sleep(7000);
    assertTrue(monitor1.isPrimary());
  }

  @Test
  public void testMonitorTakesOverAfterPrimaryDisappears() throws Exception {
    monitor1 = new PrimaryMonitor.Builder(db).build();
    Thread.sleep(7000);
    assertTrue(monitor1.isPrimary());
    monitor2 = new PrimaryMonitor.Builder(db).build();
    monitor1.shutdown();
    Thread.sleep(7000);
    assertTrue(monitor2.isPrimary());
  }

  private static class StringDatabase implements PrimaryMonitorDatabase {

    private volatile String value = "";

    @Override
    public String readValue() {
      return value;
    }

    @Override
    public void writeValue(String value) {
      this.value = value;
    }
  }
}
