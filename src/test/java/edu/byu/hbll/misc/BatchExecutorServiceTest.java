/** */
package edu.byu.hbll.misc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/** */
@RunWith(MockitoJUnitRunner.class)
public class BatchExecutorServiceTest {

  BatchExecutorService<Integer, Integer> executor;
  BrImpl r = new BrImpl();

  @Mock ThreadFactory mockedThreadFactory;

  /** @throws java.lang.Exception */
  @Before
  public void setUp() throws Exception {
    executor = new BatchExecutorService.Builder(r).build();
  }

  /** @throws java.lang.Exception */
  @After
  public void tearDown() throws Exception {
    executor.shutdownAndWait();
  }

  /**
   * Test method for {@link
   * edu.byu.lib.util.BatchExecutorService#BatchExecutor(edu.byu.lib.util.BatchExecutorService.Builder)}.
   */
  @Test
  public void testBatchExecutorService() {
    new BatchExecutorService.Builder(r).build();
    BatchRunnable<Integer, Double> r =
        b -> b.stream().map(e -> Math.pow(e, 2)).collect(Collectors.toList());
    new BatchExecutorService.Builder(r).build();
  }

  /**
   * Test method for {@link
   * edu.byu.lib.util.BatchExecutorService#BatchExecutor(edu.byu.lib.util.BatchExecutorService.Builder)}.
   */
  @Test
  public void testBatchExecutorServiceBadThreadFactory() {
    when(mockedThreadFactory.newThread(any())).thenReturn(null);

    try {
      new BatchExecutorService.Builder(r).threadFactory(mockedThreadFactory).threadCount(2).build();
      fail("exception should have been thrown");
    } catch (NullPointerException expected) {
    }
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#shutdown()}. */
  @Test
  public void testShutdown() {
    executor.shutdown();
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#shutdownAndWait()}. */
  @Test
  public void testShutdownAndWait() throws Exception {
    int sum = 0;

    for (int i = 0; i < 10001; i++) {
      executor.submit(i);
      sum += i;
    }

    executor.shutdownAndWait();

    assertEquals(sum, r.i.get());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#getQueue()}. */
  @Test
  public void testGetQueue() throws Exception {
    executor.getQueue().put(1);
    assertTrue(executor.getQueue().offer(2));
    assertTrue(executor.getQueue().offer(3, 1, TimeUnit.MILLISECONDS));

    Thread.sleep(200);

    assertEquals(6, r.i.get());

    executor.shutdown();
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#getQueue()}. */
  @Test
  public void testGetQueueFull() throws Exception {
    executor.shutdown();
    r = new BrImpl(1000);
    executor = new BatchExecutorService.Builder(r).queueCapacity(1).build();
    // insert one to initiate sleep
    executor.submit(0);
    Thread.sleep(100);

    executor.getQueue().put(1);
    executor.getQueue().put(1);

    assertTrue(executor.getQueue().offer(1, 1, TimeUnit.DAYS));
    assertFalse(executor.getQueue().offer(10, 0, TimeUnit.MILLISECONDS));
    assertFalse(executor.getQueue().offer(10));

    executor.shutdownAndWait();

    assertEquals(3, r.i.get());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#getQueue()}. */
  @Test
  public void testGetQueueMetadata() throws Exception {
    executor.shutdown();
    executor = new BatchExecutorService.Builder(new BrImpl(1000)).build();
    // insert one to initiate sleep
    executor.submit(0);
    Thread.sleep(100);

    executor.submit(1);
    executor.submit(2);
    executor.submit(3);

    assertEquals(3, executor.getQueue().size());
    assertEquals(97, executor.getQueue().remainingCapacity());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#getQueue()}. */
  @Test
  public void testGetQueueRemove() throws Exception {
    executor.shutdown();
    executor = new BatchExecutorService.Builder(new BrImpl(1000)).build();
    // insert one to initiate sleep
    executor.submit(0);
    Thread.sleep(100);

    BlockingQueue<Integer> queue = executor.getQueue();

    executor.submit(1);
    assertEquals(1, (int) queue.poll());
    assertNull(queue.poll());

    executor.submit(1);
    assertEquals(1, (int) queue.poll(1, TimeUnit.MILLISECONDS));
    assertNull(queue.poll(1, TimeUnit.MILLISECONDS));

    executor.submit(1);
    assertEquals(1, (int) queue.take());

    executor.submit(1);
    assertEquals(1, (int) queue.peek());
    assertEquals(1, (int) queue.poll());
    assertNull(queue.peek());

    executor.submit(1);
    executor.submit(2);
    executor.submit(3);
    List<Integer> dump = new ArrayList<>();
    queue.drainTo(dump);
    assertEquals(new ArrayList<>(Arrays.asList(1, 2, 3)), dump);
    assertEquals(0, queue.size());

    executor.submit(1);
    executor.submit(2);
    executor.submit(3);
    dump = new ArrayList<>();
    queue.drainTo(dump, 2);
    assertEquals(new ArrayList<>(Arrays.asList(1, 2)), dump);
    assertEquals(1, queue.size());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#getQueue()}. */
  @Test
  public void testGetQueueIterator() throws Exception {
    executor.shutdown();
    executor = new BatchExecutorService.Builder(new BrImpl(1000)).build();
    // insert one to initiate sleep
    executor.submit(0);
    Thread.sleep(100);

    BlockingQueue<Integer> queue = executor.getQueue();
    executor.submit(1);
    executor.submit(2);
    executor.submit(3);

    List<Integer> dump = new ArrayList<>();
    Iterator<Integer> it = queue.iterator();

    while (it.hasNext()) {
      dump.add(it.next());
    }

    assertEquals(new ArrayList<>(Arrays.asList(1, 2, 3)), dump);
    assertEquals(3, queue.size());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#getQueue()}. */
  @Test
  public void testGetQueueAfterShutdown() throws Exception {
    executor.shutdown();
    executor.getQueue().put(1);
    assertFalse(executor.getQueue().offer(2));
    assertFalse(executor.getQueue().offer(3, 1, TimeUnit.MILLISECONDS));

    Thread.sleep(200);

    assertEquals(0, r.i.get());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#submit(java.lang.Object)}. */
  @Test
  public void testSubmit() throws Exception {
    Future<Integer> f1 = executor.submit(1);
    Future<Integer> f2 = executor.submit(2);
    Future<Integer> f3 = executor.submit(3);

    assertEquals(1, f1.get().intValue());
    assertEquals(3, f2.get().intValue());
    assertEquals(6, f3.get().intValue());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService#submit(java.lang.Object)}. */
  @Test
  public void testSubmitAfterShutdown() throws Exception {
    executor.shutdown();

    try {
      executor.submit(1);
      fail("should throw exception");
    } catch (RejectedExecutionException expected) {
    }
  }

  /**
   * Test method for {@link edu.byu.lib.util.BatchExecutorService#submitAll(java.util.Collection)}.
   */
  @Test
  public void testSubmitAll() throws Exception {
    List<Future<Integer>> fs = executor.submitAll(Arrays.asList(1, 2, 3));
    Iterator<Integer> it = Arrays.asList(1, 3, 6).iterator();

    for (Future<Integer> f : fs) {
      assertEquals(it.next().intValue(), f.get().intValue());
    }
  }

  @Test
  public void testSuspend() throws Exception {
    executor.shutdownAndWait();
    executor = new BatchExecutorService.Builder(r).batchCapacity(1).build();

    Future<Integer> f1 = executor.submit(1);
    Future<Integer> f2 = executor.submit(2);
    Future<Integer> f3 = executor.submit(3);

    Thread.sleep(500);

    assertTrue(f1.isDone());
    assertTrue(f2.isDone());
    assertTrue(f3.isDone());

    executor.shutdownAndWait();
    executor =
        new BatchExecutorService.Builder(r)
            .batchCapacity(1)
            .suspend(Duration.ofMillis(100))
            .build();

    // prime it
    executor.submit(0);
    Thread.sleep(500);

    f1 = executor.submit(1);
    f2 = executor.submit(2);
    f3 = executor.submit(3);

    Thread.sleep(50);

    assertTrue(f1.isDone());
    assertFalse(f2.isDone());
    assertFalse(f3.isDone());

    Thread.sleep(100);

    assertTrue(f2.isDone());
    assertFalse(f3.isDone());

    Thread.sleep(100);

    assertTrue(f3.isDone());
  }

  @Test
  public void testStability() throws Exception {
    load(1, 1, 1);
    load(4, 1, 1);
    load(4, 1000, 1);
    load(4, 1000, 4);
  }

  private void load(int threadCount, int batchCapacity, int submitterCount) throws Exception {
    executor.shutdownAndWait();
    executor =
        new BatchExecutorService.Builder(r)
            .threadCount(threadCount)
            .batchCapacity(batchCapacity)
            .build();
    r.i.set(0);

    int total = 1000000;
    CountDownLatch latch = new CountDownLatch(submitterCount);

    for (int i = 0; i < submitterCount; i++) {
      Thread thread =
          new Thread(
              () -> {
                for (int j = 0; j < total; j++) {
                  try {
                    executor.submit(1);
                  } catch (InterruptedException e) {
                  }
                }

                latch.countDown();
              });

      thread.start();
    }

    latch.await();
    executor.shutdownAndWait();

    assertEquals(total * submitterCount, r.i.get());
  }

  @Test
  public void testCodeInJavadoc() throws Exception {
    // sample code start
    BatchRunnable<Integer, Double> r =
        b -> b.stream().map(e -> Math.pow(e, 2)).collect(Collectors.toList());
    BatchExecutorService<Integer, Double> executor = new BatchExecutorService.Builder(r).build();

    Future<Double> f1 = executor.submit(1);
    Future<Double> f2 = executor.submit(2);
    Future<Double> f3 = executor.submit(3);

    System.out.println(f1.get());
    System.out.println(f2.get());
    System.out.println(f3.get());

    executor.shutdownAndWait();
    // sample code end

    assertEquals(1, (int) (double) f1.get());
    assertEquals(4, (int) (double) f2.get());
    assertEquals(9, (int) (double) f3.get());
  }

  /**
   * When a shutdown occurs, internal worker threads are interrupted. This is a huge problem if the
   * interruption happens while a worker thread is executing a batch. Threads in an "executing"
   * state should NOT be interrupted. Interrupts should only happen during the "filling" state.
   *
   * <p>To test this scenario properly, the runnable must block.
   */
  @Test
  public void testBlockedTaskAtShutdown() throws Exception {
    executor.shutdownAndWait();

    BatchRunnable<Integer, Integer> r =
        batch -> {
          try {
            Thread.sleep(100);
            return null;
          } catch (InterruptedException e) {
            throw new RuntimeException(e);
          }
        };

    executor = new BatchExecutorService.Builder(r).threadCount(2).build();
    Future<Integer> future = executor.submit(0);
    executor.shutdownAndWait();
    future.get();
  }

  /** */
  static class BrImpl implements BatchRunnable<Integer, Integer> {

    AtomicInteger i = new AtomicInteger();
    long sleep;

    public BrImpl() {}

    public BrImpl(long sleep) {
      this.sleep = sleep;
    }

    @Override
    public List<Integer> run(List<Integer> batch) {
      return batch
          .stream()
          .map(
              e -> {
                if (sleep > 0) {
                  try {
                    Thread.sleep(sleep);
                  } catch (InterruptedException e1) {
                  }
                }

                return i.addAndGet(e);
              })
          .collect(Collectors.toList());
    }
  }
}
