package edu.byu.hbll.misc;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Test;

/**
 * Unit tests for {@link Streams}.
 */
public class StreamsTest {

  /**
   * Verifies that attempting to run {@link Streams#of(Iterable)} with a {@code null}
   * {@link Iterable} will fail.
   */
  @Test(expected = NullPointerException.class)
  public void shouldFailToCreateStreamFromNullIterable() {
    Streams.of((Iterable<String>) null);
  }

  /**
   * Verifies that {@link Streams#of(Iterable)} will create a new {@link Stream} from the given
   * {@link Iterable}.
   */
  @Test
  public void shouldCreateStreamFromIterable() {
    Iterable<String> iterable = Arrays.asList("a", "b", "c");

    Set<String> set = Streams.of(iterable).collect(Collectors.toSet());
    assertEquals(3, set.size());
  }

  /**
   * Verifies that attempting to run {@link Streams#of(Iterator)} with a {@code null}
   * {@link Iterator} will fail.
   */
  @Test(expected = NullPointerException.class)
  public void shouldFailToCreateStreamFromNullIterator() {
    Streams.of((Iterator<String>) null);
  }

  /**
   * Verifies that {@link Streams#of(Iterator)} will create a new {@link Stream} from the given
   * {@link Iterator}.
   */
  @Test
  public void shouldCreateStreamFromIterator() {
    Iterator<String> iterator = Arrays.asList("a", "b", "c").iterator();

    Set<String> set = Streams.of(iterator).collect(Collectors.toSet());
    assertEquals(3, set.size());
  }

}
