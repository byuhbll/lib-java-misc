package edu.byu.hbll.misc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Test;

/**
 * Unit tests for {@link Lists}.
 */
public class ListsTest {

  private static final String FIRST = "a";
  private static final String SECOND = "b";
  private static final String THIRD = "c";

  private static final List<String> NULL = null;
  private static final List<String> EMPTY = Collections.emptyList();
  private static final List<String> SINGLE = Collections.singletonList(FIRST);
  private static final List<String> MULTIPLE = Arrays.asList(FIRST, SECOND);

  /**
   * Verifies that calling {@link Lists#first(List)} with a {@code null} list will return an empty
   * {@link Optional}.
   */
  @Test
  public void firstShouldReturnEmptyIfListIsNull() {
    assertFalse(Lists.first(NULL).isPresent());
  }

  /**
   * Verifies that calling {@link Lists#first(List)} with an empty list will return an empty
   * {@link Optional}.
   */
  @Test
  public void firstShouldReturnEmptyIfListIsEmpty() {
    assertFalse(Lists.first(EMPTY).isPresent());
  }

  /**
   * Verifies that calling {@link Lists#first(List)} with a list containing a single element will
   * return that element as an {@link Optional}.
   */
  @Test
  public void firstShouldReturnTheOnlyElementInAList() {
    assertEquals(FIRST, Lists.first(SINGLE).get());
  }

  /**
   * Verifies that calling {@link Lists#first(List)} with a list containing a multiple elements will
   * return the first element as an {@link Optional}.
   */
  @Test
  public void firstShouldReturnTheFirstElementInAList() {
    assertEquals(FIRST, Lists.first(MULTIPLE).get());
  }

  /**
   * Verifies that calling {@link Lists#last(List)} with a {@code null} list will return an empty
   * {@link Optional}.
   */
  @Test
  public void lastShouldReturnEmptyIfListIsNull() {
    assertFalse(Lists.last(NULL).isPresent());
  }

  /**
   * Verifies that calling {@link Lists#last(List)} with an empty list will return an empty
   * {@link Optional}.
   */
  @Test
  public void lastShouldReturnEmptyIfListIsEmpty() {
    assertFalse(Lists.last(EMPTY).isPresent());
  }

  /**
   * Verifies that calling {@link Lists#last(List)} with a list containing a single element will
   * return that element as an {@link Optional}.
   */
  @Test
  public void lastShouldReturnTheOnlyElementInAList() {
    assertEquals(FIRST, Lists.last(SINGLE).get());
  }

  /**
   * Verifies that calling {@link Lists#last(List)} with a list containing a multiple elements will
   * return the last element as an {@link Optional}.
   */
  @Test
  public void lastShouldReturnTheFirstElementInAList() {
    assertEquals(SECOND, Lists.last(MULTIPLE).get());
  }

  /**
   * Verifies that calling {@link Lists#with(List, Object...)} with a {@code null} list and no new
   * elements will return an empty list.
   */
  @Test
  public void withShouldReturnEmptyListIfNullListAndNoNewElementsAreProvided() {
    List<String> newList = Lists.with(NULL);
    assertEquals(0, newList.size());
  }

  /**
   * Verifies that calling {@link Lists#with(List, Object...)} with an empty list and no new
   * elements will return an empty list.
   */
  @Test
  public void withShouldReturnEmptyListIfEmptyListAndNoNewElementsAreProvided() {
    List<String> newList = Lists.with(EMPTY);
    assertEquals(0, newList.size());
  }

  /**
   * Verifies that calling {@link Lists#with(List, Object...)} with an empty list (that is
   * specifically not {@link Collections#emptyList} and no new elements will return a new empty
   * list.
   */
  @Test
  public void withShouldReturnEmptyListIfCustomEmptyListAndNoNewElementsAreProvided() {
    List<String> origList = new ArrayList<>();
    List<String> newList = Lists.with(origList);

    assertFalse(newList == origList);
    assertEquals(0, newList.size());
  }

  /**
   * Verifies that calling {@link Lists#with(List, Object...)} with a {@code null} list and a single
   * new element will return a new list containing only the new element.
   */
  @Test
  public void withShouldReturnNewListWhenNullListIsProvided() {
    List<String> newList = Lists.with(NULL, THIRD);

    assertFalse(newList == NULL);
    assertEquals(1, newList.size());
    assertEquals(THIRD, newList.get(0));
  }

  /**
   * Verifies that calling {@link Lists#with(List, Object...)} with an empty list and a single new
   * element will return a new list containing only the new element.
   */
  @Test
  public void withShouldReturnNewListWhenEmptyListIsProvided() {
    List<String> newList = Lists.with(EMPTY, THIRD);

    assertFalse(newList == EMPTY);
    assertEquals(1, newList.size());
    assertEquals(THIRD, newList.get(0));
  }

  /**
   * Verifies that calling {@link Lists#with(List, Object...)} with a populated list and a single
   * new element will return a new list containing the original list appended with the new element.
   */
  @Test
  public void withShouldReturnNewListWhenNonEmptyListIsProvided() {
    List<String> newList = Lists.with(MULTIPLE, THIRD);

    assertFalse(newList == MULTIPLE);
    assertEquals(3, newList.size());
    assertEquals(FIRST, newList.get(0));
    assertEquals(SECOND, newList.get(1));
    assertEquals(THIRD, newList.get(2));
  }

  /**
   * Verifies that calling {@link Lists#with(List, Object...)} with a populated list and multiple
   * new elements will return a new list containing the original list appended with the new
   * elements.
   */
  @Test
  public void withShouldReturnNewListWhenNonEmptyListAndMultipleNewElementsAreProvided() {
    List<String> newList = Lists.with(SINGLE, SECOND, THIRD);
    assertFalse(newList == SINGLE);
    assertEquals(FIRST, newList.get(0));
    assertEquals(SECOND, newList.get(1));
    assertEquals(THIRD, newList.get(2));
  }

  /**
   * Verifies that calling {@link Lists#with(List, Object...)} will return an unmodifiable list.
   */
  @Test(expected = UnsupportedOperationException.class)
  public void withShouldReturnImmutableList() {
    List<String> newList = Lists.with(new ArrayList<>(), FIRST);
    newList.add(SECOND);
  }

}
