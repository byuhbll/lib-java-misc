package edu.byu.hbll.misc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Optional;
import org.junit.Test;

/**
 * Unit tests for {@link Strings}.
 */
public class StringsTest {

  private static final String NULL = null;
  private static final String EMPTY = "";
  private static final String WHITESPACE = "  \t";
  private static final String TRIMMABLE = "  test\t";
  private static final String TRIMMED = "test";

  /**
   * Verifies that calling {@link Strings#trimToOptional(String)} with a {@code null} string will
   * return an empty {@link Optional}.
   */
  @Test
  public void trimToOptionalShouldReturnEmptyIfStringIsNull() {
    assertFalse(Strings.trimToOptional(NULL).isPresent());
  }

  /**
   * Verifies that calling {@link Strings#trimToOptional(String)} with an empty string will return
   * an empty {@link Optional}.
   */
  @Test
  public void trimToOptionalShouldReturnEmptyIfStringIsEmpty() {
    assertFalse(Strings.trimToOptional(EMPTY).isPresent());
  }

  /**
   * Verifies that calling {@link Strings#trimToOptional(String)} with an non-empty string
   * containing only whitespace will return an empty {@link Optional}.
   */
  @Test
  public void trimToOptionalShouldReturnEmptyIfStringIsOnlyWhitespace() {
    assertFalse(Strings.trimToOptional(WHITESPACE).isPresent());
  }

  /**
   * Verifies that calling {@link Strings#trimToOptional(String)} with a trimmable string will
   * return an {@link Optional} of the trimmed version of that string.
   */
  @Test
  public void trimToOptionalShouldReturnTrimmedString() {
    assertEquals(TRIMMED, Strings.trimToOptional(TRIMMABLE).get());
  }

  /**
   * Verifies that calling {@link Strings#trimToOptional(String)} with an already-trimmed string
   * will return the original string.
   */
  @Test
  public void trimToOptionalShouldReturnOriginalStringIfAlreadyTrimmed() {
    assertEquals(TRIMMED, Strings.trimToOptional(TRIMMED).get());
  }

  /**
   * Verifies that calling {@link Strings#isBlank(String)} with a {@code null} string will return
   * {@code true}.
   */
  @Test
  public void isBlankShouldReturnTrueIfStringIsNull() {
    assertTrue(Strings.isBlank(NULL));
  }

  /**
   * Verifies that calling {@link Strings#isBlank(String)} with an empty string will return {@code
   * true}.
   */
  @Test
  public void isBlankShouldReturnTrueIfStringIsEmpty() {
    assertTrue(Strings.isBlank(EMPTY));
  }

  /**
   * Verifies that calling {@link Strings#isBlank(String)} with a non-empty string containing only
   * whitespace will return {@code true}.
   */
  @Test
  public void isBlankShouldReturnTrueIfStringIsOnlyWhitespace() {
    assertTrue(Strings.isBlank(WHITESPACE));
  }

  /**
   * Verifies that calling {@link Strings#isBlank(String)} with a string containing non-whitespace
   * characters returns {@code false}.
   */
  @Test
  public void isBlankShouldReturnFalseIfStringHasNonWhitespace() {
    assertFalse(Strings.isBlank(TRIMMABLE));
  }

  /**
   * Verifies that calling {@link Strings#requireNonBlank(String)} with a {@code null} string will
   * throw an {@link IllegalArgumentException}.
   */
  @Test(expected = IllegalArgumentException.class)
  public void requireNonBlankShouldFailIfStringIsNull() {
    Strings.requireNonBlank(NULL);
  }

  /**
   * Verifies that calling {@link Strings#requireNonBlank(String)} with an empty string will throw
   * an {@link IllegalArgumentException}.
   */
  @Test(expected = IllegalArgumentException.class)
  public void requireNonBlankShouldFailIfStringIsEmpty() {
    Strings.requireNonBlank(EMPTY);
  }

  /**
   * Verifies that calling {@link Strings#requireNonBlank(String)} with a non-empty string
   * containing only whitespace will throw an {@link IllegalArgumentException}.
   */
  @Test(expected = IllegalArgumentException.class)
  public void requireNonBlankShouldFailIfStringIsOnlyWhitespace() {
    Strings.requireNonBlank(WHITESPACE);
  }

  /**
   * Verifies that calling {@link Strings#requireNonBlank(String)} with a string containing
   * non-whitespace characters will pass.
   */
  @Test
  public void requireNonBlankShouldFailIfStringHasNonWhitespace() {
    Strings.requireNonBlank(TRIMMABLE);
  }

  /**
   * Verifies that {@link Strings#requireNonBlank(String)} will use the provided message upon
   * failure.
   */
  @Test
  public void requireNonBlankShouldFailWithGivenMessage() {
    try {
      Strings.requireNonBlank(NULL, TRIMMED);
      fail("IllegalArgumentException expected, but none was thrown.");
    } catch (IllegalArgumentException e) {
      assertEquals(TRIMMED, e.getMessage());
    }
  }

  /**
   * Verifies that {@link Strings#requireNonBlank(String)} will use the provided message provider
   * upon failure.
   */
  @Test
  public void requireNonBlankShouldFailWithGivenMessageProvider() {
    try {
      Strings.requireNonBlank(NULL, () -> TRIMMED);
      fail("IllegalArgumentException expected, but none was thrown.");
    } catch (IllegalArgumentException e) {
      assertEquals(TRIMMED, e.getMessage());
    }
  }
}
