/** */
package edu.byu.hbll.misc;

import static org.junit.Assert.*;

import java.time.Duration;

import org.junit.After;
import org.junit.Test;

import edu.byu.hbll.misc.BatchExecutorServiceTest.BrImpl;

/** */
public class BatchExecutorServiceBuilderTest {

  BatchExecutorService<Integer, Integer> executor1;
  BatchExecutorService<Integer, Integer> executor2;

  /** @throws java.lang.Exception */
  @After
  public void tearDown() throws Exception {
    if (executor1 != null) executor1.shutdown();
    if (executor2 != null) executor2.shutdown();
  }

  /**
   * Test method for {@link
   * edu.byu.lib.util.BatchExecutorService.Builder#Builder(edu.byu.lib.util.BatchRunnable)}.
   */
  @Test
  public void testBuilder() {
    executor1 = new BatchExecutorService.Builder(new BrImpl()).build();
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService.Builder#queueCapacity(int)}. */
  @Test
  public void testQueueCapacity() {
    executor1 = new BatchExecutorService.Builder(new BrImpl()).build();
    assertEquals(100, executor1.getQueueCapacity());
    executor2 = new BatchExecutorService.Builder(new BrImpl()).queueCapacity(17).build();
    assertEquals(17, executor2.getQueueCapacity());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService.Builder#batchCapacity(int)}. */
  @Test
  public void testBatchCapacity() {
    executor1 = new BatchExecutorService.Builder(new BrImpl()).build();
    assertEquals(10, executor1.getBatchCapacity());
    executor2 = new BatchExecutorService.Builder(new BrImpl()).batchCapacity(17).build();
    assertEquals(17, executor2.getBatchCapacity());
  }

  /**
   * Test method for {@link
   * edu.byu.lib.util.BatchExecutorService.Builder#batchDelay(java.time.Duration)}.
   */
  @Test
  public void testBatchDelay() {
    executor1 = new BatchExecutorService.Builder(new BrImpl()).build();
    assertEquals(Duration.ZERO, executor1.getBatchDelay());
    executor2 =
        new BatchExecutorService.Builder(new BrImpl()).batchDelay(Duration.ofMillis(100)).build();
    assertEquals(Duration.ofMillis(100), executor2.getBatchDelay());
  }

  /** Test method for {@link edu.byu.lib.util.BatchExecutorService.Builder#threadCount(int)}. */
  @Test
  public void testThreadCount() {
    executor1 = new BatchExecutorService.Builder(new BrImpl()).build();
    assertEquals(1, executor1.getThreadCount());
    executor2 = new BatchExecutorService.Builder(new BrImpl()).threadCount(2).build();
    assertEquals(2, executor2.getThreadCount());
  }

  /**
   * Test method for {@link
   * edu.byu.lib.util.BatchExecutorService.Builder#suspend(java.time.Duration)}.
   */
  @Test
  public void testSuspend() {
    executor1 = new BatchExecutorService.Builder(new BrImpl()).build();
    assertEquals(Duration.ZERO, executor1.getSuspend());
    executor2 =
        new BatchExecutorService.Builder(new BrImpl()).suspend(Duration.ofSeconds(1)).build();
    assertEquals(Duration.ofSeconds(1), executor2.getSuspend());
  }
}
